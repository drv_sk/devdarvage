user  nginx;
worker_processes  1;

error_log  /var/log/nginx/error.log warn;
pid        /var/run/nginx.pid;

events {
  worker_connections  1024;
}

http {
  include       /etc/nginx/mime.types;
  default_type  application/octet-stream;

  log_format  main  '$remote_addr - $remote_user [$time_local] "$request" '
                    '$status $body_bytes_sent "$http_referer" '
                    '"$http_user_agent" "$http_x_forwarded_for"';
  map $http_upgrade $connection_upgrade {
        default upgrade;
        '' close;
    }

  access_log  /var/log/nginx/access.log  main;

  sendfile            on;
  keepalive_timeout   65;


  client_max_body_size 500m;


  # For load balancing to multiple backend servers, add servers
  # See http://nginx.org/en/docs/http/load_balancing.html

  server {
    listen 80;
    listen [::]:80;

    server_name orders.messer.com.mk;

    root /usr/share/nginx/html;

    location / {
          # Someday: transition to Forwarded header defined in https://tools.ietf.org/html/rfc7239
          proxy_set_header  Host              $http_host;
          proxy_set_header  X-Real-IP         $remote_addr;
          proxy_set_header  X-Forwarded-For   $proxy_add_x_forwarded_for;
          proxy_set_header  X-Forwarded-Proto $scheme;
          proxy_pass        http://ms-backend;
          proxy_intercept_errors  off;
        }
    # Forward api requests to the backend
    location /api {
      # Someday: transition to Forwarded header defined in https://tools.ietf.org/html/rfc7239
      proxy_set_header  Host              $http_host;
      proxy_set_header  X-Real-IP         $remote_addr;
      proxy_set_header  X-Forwarded-For   $proxy_add_x_forwarded_for;
      proxy_set_header  X-Forwarded-Proto $scheme;
      proxy_pass        http://ms-backend;
      proxy_intercept_errors  off;
    }

    location ~* \.io {
          proxy_set_header X-Real-IP $remote_addr;
          proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
          proxy_set_header Host $http_host;
          proxy_set_header X-NginX-Proxy true;

          proxy_pass http://ms-socket:3000;
          proxy_redirect off;

          proxy_http_version 1.1;
          proxy_set_header Upgrade $http_upgrade;
          proxy_set_header Connection "upgrade";
        }
  }

  server {
      listen 3000;
      listen [::]:3000;

      server_name orders.messer.com.mk;

      root /usr/share/nginx/html;

      location ~* \.io {
            proxy_set_header X-Real-IP $remote_addr;
            proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
            proxy_set_header Host $http_host;
            proxy_set_header X-NginX-Proxy true;

            proxy_pass http://ms-socket:3000;
            proxy_redirect off;

            proxy_http_version 1.1;
            proxy_set_header Upgrade $http_upgrade;
            proxy_set_header Connection "upgrade";
          }
    }

    server {
            listen 80;
            listen [::]:80;
            server_name www.capitalexpert.mk;
            return 301 $scheme://capitalexpert.mk$request_uri;
    }

    server {
        listen 80;
        listen [::]:80;

        server_name capitalexpert.mk;

        root /usr/share/nginx/html;

        location / {
              # Someday: transition to Forwarded header defined in https://tools.ietf.org/html/rfc7239
              proxy_set_header  Host              $http_host;
              proxy_set_header  X-Real-IP         $remote_addr;
              proxy_set_header  X-Forwarded-For   $proxy_add_x_forwarded_for;
              proxy_set_header  X-Forwarded-Proto $scheme;
              proxy_pass        http://ce-frontend;
              proxy_intercept_errors  off;
            }
        # Forward api requests to the backend
        location /api {
          # Someday: transition to Forwarded header defined in https://tools.ietf.org/html/rfc7239
          proxy_set_header  Host              $http_host;
          proxy_set_header  X-Real-IP         $remote_addr;
          proxy_set_header  X-Forwarded-For   $proxy_add_x_forwarded_for;
          proxy_set_header  X-Forwarded-Proto $scheme;
          proxy_pass        http://ce-backend;
          proxy_intercept_errors  off;
        }

      }

}
